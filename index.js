//console.log("Hello World!");


/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:


	let getInfo = function(){
		alert("Enter your Information");
			let Name = prompt("Enter your Full Name");
			let Age = prompt("Enter your Age");
			let Loc = prompt("Enter your Location");
	console.log("Hello, " + Name);
	console.log("You are, " + Age);
	console.log("You live in, " + Loc);
}

getInfo();

console.log("---------------");
/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	let getFavArtist = function(){
		alert("Enter your top 5 favorite bands/musical artists");
			let artist1 = prompt("1st favorite bands/musical artists");
			let artist2 = prompt("2nd favorite bands/musical artists");
			let artist3 = prompt("3rd favorite bands/musical artists");
			let artist4 = prompt("4th favorite bands/musical artists");
			let artist5 = prompt("5th favorite bands/musical artists");
		console.log("Your Top 5 avorite bands/musical artists are:");
		console.log("1. " + artist1);
		console.log("2. " + artist2);
		console.log("3. " + artist3);
		console.log("4. " + artist4);
		console.log("5. " + artist5);
	}

	getFavArtist();

console.log("--------------");
/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function printFavMovies(){
	console.log("My top favorite movies are: ")
		console.log("1. Avengers End Game");
		console.log("rotten tomatoes rating: 94%");

		console.log("1. Pearl Harbor");
		console.log("rotten tomatoes rating: 24%");

		console.log("1. American Pie");
		console.log("rotten tomatoes rating: 61%");

		console.log("1. Interstellar");
		console.log("rotten tomatoes rating: 73%");

		console.log("1. The Big Bang Theory");
		console.log("rotten tomatoes rating: 81%");


}

printFavMovies();
console.log("------------");

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/







let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

//console.log(friend1);
//console.log(friend2);